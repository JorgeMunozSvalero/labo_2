package jorge.munoz.API;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jorge.munoz.Model.Movie;

@RestController
public class MoviesController {


    private static ArrayList<Movie> moviesList = new ArrayList<Movie>() {
        {
            add(new Movie(1, "Lagrimas de sangre 2",   2020, "https://picsum.photos/200/300",   "Super description of a wonderfoul film that you should see. Highly recommendable if you like action movies (any kind of movie tbh) See you soon at the greatest theaters ;)  ", "Maria Pesa"));
            add(new Movie(1, "Lagrimas de sangre",     2009, "https://picsum.photos/200/300",   "Super description of a wonderfoul film that you should see. Highly recommendable if you like action movies (any kind of movie tbh) See you soon at the greatest theaters ;)  ", "Maria Pesa"));
            add(new Movie(2, "Lo imposible",           2015, "https://picsum.photos/200/301",   "Super description of a wonderfoul film that you should see. Highly recommendable if you like action movies (any kind of movie tbh) See you soon at the greatest theaters ;)  ", "Laura Perez"));
            add(new Movie(2, "Lo mejor",               2016, "http://placekitten.com/200/300", "Super description of a wonderfoul film that you should see. Highly recommendable if you like action movies (any kind of movie tbh) See you soon at the greatest theaters ;)   ", "Hortensia Rodrigo"));
            add(new Movie(3, "007. Imparable",         2015, "http://placekitten.com/200/301",   "Super description of a wonderfoul film that you should see. Highly recommendable if you like action movies (any kind of movie tbh) See you soon at the greatest theaters ;) ", "Espléndido Castro"));
            add(new Movie(4, "Chuck Norris revenge",   2020, "https://picsum.photos/200/302",   "Super description of a wonderfoul film that you should see. Highly recommendable if you like action movies (any kind of movie tbh) See you soon at the greatest theaters ;)  ", "Carlos Segundo"));
            add(new Movie(5, "Chuck Norris revenge 2", 2000, "http://placekitten.com/200/300",   "Super description of a wonderfoul film that you should see. Highly recommendable if you like action movies (any kind of movie tbh) See you soon at the greatest theaters ;) ", "Jesusito Arriba"));
            add(new Movie(6, "Homeland Security",      2002, "https://picsum.photos/199/299",   "Super description of a wonderfoul film that you should see. Highly recommendable if you like action movies (any kind of movie tbh) See you soon at the greatest theaters ;)  ", "Kevin Campos"));
        }
    };

    public static Movie getMovieById(Integer id) {
        for (Movie movie : moviesList) {
            if (movie.getId() == (int) id) {
                return movie;
            }
        }
        return null;
    }

    /**
     * Get Movie(s) from a list.
     * 
     * @author JorgeM
     * @param title Title of the movie to search (Optional)
     * @return ArrayList with all movies if no Params, or arraylist with the movies
     *         or movie founded.
     * @throws NotDataFoundException
     */
    private static ArrayList<Movie> getMovieByTitle(String title) {
        boolean foundTitle = false;
        if (title == null) {
            return moviesList;
        }
        ArrayList<Movie> moviesByTitle = new ArrayList<>();
        for (Movie movie : moviesList) {
            String movieTitleLower = movie.getTitle().toLowerCase();
            String movieSerachTitleLower = title.toLowerCase();
            if (movieTitleLower.contains(movieSerachTitleLower)) {
                moviesByTitle.add(movie);
                foundTitle = true;
            }
        }
        if (foundTitle)
            return moviesByTitle;
        return null;
    }

    /**
     * Get Movie(s) from a list.
     * 
     * @author JorgeM
     * @param list All list of movies or already filtered by title
     * @param yr   Year of the movie (Optional)
     * @return ArrayList with all movies if no Params, or arraylist with the movies
     *         or movie founded.
     * @throws NotDataFoundException
     */
    private static ArrayList<Movie> getMovieByYear(ArrayList<Movie> list, Integer yr) {

        boolean foundYear = false;
        ArrayList<Movie> movies = new ArrayList<>();

        if (yr == null) {
            return list;
        }
        for (Movie movie : list) {
            if (movie.getYear() == (int) yr) {
                movies.add(movie);
                foundYear = true;
            }
        }
        if (foundYear)
            return movies;
        return null;
    }

    public static ArrayList<Movie> findMovieFiltered(String title, Integer year) {
        if ((title != null) && (year != null)) {
            ArrayList<Movie> moviesFilteredTitle = new ArrayList<>();
            moviesFilteredTitle = getMovieByTitle(title);
            return getMovieByYear(moviesFilteredTitle, year);
        }
        if ((title != null) || (year != null)) {
            ArrayList<Movie> moviesFilteredTitle = new ArrayList<>();
            moviesFilteredTitle = getMovieByTitle(title);
            return getMovieByYear(moviesFilteredTitle, year);
        }
        return moviesList;
    }

    @GetMapping("/movie")
    public ArrayList<Movie> getMovies(@RequestParam(name = "title", required = false) String title,
            @RequestParam(name = "year", required = false) Integer year) {

        if ((title != null) && (year != null)) {
            ArrayList<Movie> moviesFilteredTitle = new ArrayList<>();
            moviesFilteredTitle = getMovieByTitle(title);
            return getMovieByYear(moviesFilteredTitle, year);
        }
        if ((title != null) || (year != null)) {
            ArrayList<Movie> moviesFilteredTitle = new ArrayList<>();
            moviesFilteredTitle = getMovieByTitle(title);
            return getMovieByYear(moviesFilteredTitle, year);
        }
        return moviesList;

    }

    @GetMapping("/movie/{id}")
    public static Movie getMovieDetailbyID(@PathVariable("id") int id) {
        return getMovieById(id);
    }

    /*
    CRUD METHODS
    */
    @PostMapping("/movie")
    public Movie AddMovie(@RequestBody Movie newMovie) {
        if (getMovieById(newMovie.getId()) != null) {
            return null;
        } else {
            Movie tmpMovie = new Movie();
            tmpMovie.setId(newMovie.getId());
            tmpMovie.setTitle(newMovie.getTitle());
            tmpMovie.setYear(newMovie.getYear());
            tmpMovie.setPoster(newMovie.getPoster());
            moviesList.add(tmpMovie);
            return newMovie;
        }
    }

    @PutMapping("/movie")
    public Movie UpdateMovie(@RequestBody Movie newMovie) {
        if (getMovieById(newMovie.getId()) == null) {
            return null;
        } else {
            if (newMovie.getYear() != 0)
                getMovieById(newMovie.getId()).setTitle(newMovie.getTitle());
            if (newMovie.getTitle() != null)
                getMovieById(newMovie.getId()).setTitle(newMovie.getTitle());
            if (newMovie.getYear() != 0)
                getMovieById(newMovie.getId()).setYear(newMovie.getYear());
            if (newMovie.getPoster() != null)
                getMovieById(newMovie.getId()).setPoster(newMovie.getPoster());
            return getMovieById(newMovie.getId());
        }
    }

    @DeleteMapping("/movie/{id}")
    public void Delete(@PathVariable("id") int id) {
        if (getMovieById(id) != null)
            moviesList.remove(getMovieById(id));
    }

}
