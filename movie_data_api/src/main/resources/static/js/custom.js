function reload() {
    var e = document.getElementById("languages");
    var selectedLang = e.options[e.selectedIndex].value;

    var url = "?lang=" + selectedLang
    location.href = url;
}

function loadData() {
    if (isNaN(document.getElementById('year').value)) {
        deleteData()
        var language = document.cookie.split('=')[1]
        switch (language) {
            case "es":
                alert("Comprueba los parámetros")
                break;
            case "en":
                alert("Ccheck the params")
                break;
            default:
                alert("Chek the params")
                break;
        }
        return
    }

    let controllerURL = "http://localhost:9191/movie"
    let search = 0
    if ((document.getElementById('title').value != "") && (document.getElementById('year').value == "")) {
        search = 1
    }
    if ((document.getElementById('title').value == "") && (document.getElementById('year').value != "")) {
        search = 2
    }
    if ((document.getElementById('title').value != "") && (document.getElementById('year').value != "")) {
        search = 3
    }
    if ((document.getElementById('title').value == "") && (document.getElementById('year').value == "")) {
        search = 0
    }

    switch (search) {
        case 1:
            controllerURL += "?title=" + document.getElementById('title').value
            break;
        case 2:
            controllerURL += '?year=' + document.getElementById('year').value
            break;
        case 3:
            controllerURL += "?title=" + document.getElementById('title').value + '&year=' + document.getElementById('year').value
            break;
        default:
            controllerURL = "http://localhost:9191/movie"
            break;
    }
    document.getElementById('infoPelis').innerHTML = ""

    fetch(controllerURL)
        .then(resultadoRaw => {

            return resultadoRaw.json();

        })
        .then(resultadoComoJson => {

            document.getElementById("resultsText").style.visibility = "visible"
            document.getElementById("infoPelis").style.visibility = "visible"
            for (i = 0; i < resultadoComoJson.length; i++) {

                document.getElementById('infoPelis').innerHTML += '<div class="container mt-5 bg-light p-4 border border-dark d-flex">' +
                    '<img src="' + resultadoComoJson[i].poster + '" alt="">' + '<div class="container-sm">' + '<h4 class="text-dark">' + resultadoComoJson[i].title + '</h4>' + '<h6 class="text-dark">' + resultadoComoJson[i].year + '</h6>' +
                    '<p class="text-dark">' + resultadoComoJson[i].info + '</p>' +
                    '</div>' +
                    '<button> Ir a detalle </button>' +
                    '</div>';
            }

        });


}

function deleteData() {
    document.getElementById('infoPelis').innerHTML = ""
    document.getElementById("title").value = ""
    document.getElementById("year").value = ""
    document.getElementById("infoPelis").style.visibility = "hidden"
    document.getElementById("resultsText").style.visibility = "hidden"
}